// ZMPOLab6Poprawa.cpp : Ten plik zawiera funkcję „main”. W nim rozpoczyna się i kończy wykonywanie programu.
//

#include "pch.h"
#include "Date.h"
#include "TimeStamp.h"
#include <iostream>

using namespace std;

int main()
{
	/*
	Date d1, d2, d3, d4;
	d2 = Date(2016, 10, 21);
	d3 = Date (1985, 5, 13);
	d4 = Date(2005, 12, 13);

	d4.addSomeDays(100);
	d2.subSomeDays(454);
	//d2.countQuantityOfDays(1980, 10, 21);
	*/

	TimeStamp ts1, ts2, ts3, ts4;
	ts2 = TimeStamp(1970, 02, 10, 10, 30);
	ts3 = TimeStamp(1970, 03, 10, 10, 20);
	ts4 = TimeStamp(1970, 02, 10, 10, 30);

	cout << ts2.getMinutes() << endl;
	ts2.subSomeMinutes(50);
	cout << ts2.getMinutes();
	/*
	cout << TimeStamp::countTimeBetweenDatesInMinutes(ts2, ts3) << endl;
	cout << TimeStamp::countTimeBetweenDatesInMinutes(ts2, ts4) << endl;
	*/

}


#pragma once
class Date
{
public:
	Date();
	Date(int year, int month, int day);
	~Date();
	double countQuantityOfDays(int year, int month, int day);
	double countTimeBetweenDates(Date firstDate, Date secondDate);
	int countQuantityOfDaysInCurrentYear(int year);
	int countQuantityOfDaysInCurrentMonth(int month);
	void addSomeDays(int howManyDays);
	void subSomeDays(int howManyDays);
	void estimateNewDate(double quantityOfDays);
	bool checkIsLeap(int year);
	bool checkIsLongerMonth(int month);
	bool checkIsFebruary(int month);
	double getDate();
	void setDate(double newDate);
	bool operator ==(Date comparedDate);
	bool operator <(Date comparedDate);
	bool operator >(Date comparedDate);
	bool operator <=(Date comparedDate);
	bool operator >=(Date comparedDate);

private:
	double date;
};


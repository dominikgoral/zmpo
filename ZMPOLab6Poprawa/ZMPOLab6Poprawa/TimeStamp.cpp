#include "pch.h"
#include "TimeStamp.h"
#include <iostream>

#define MINUTES_PER_HOUR 60
#define MINUTES_PER_DAY 1440

using namespace std;
TimeStamp::TimeStamp()
{
	minutes = 0;
	//cout << "Utworzono date rok: 1970, miesiac: 01, dzien: 01, godzina: 00, minuta: 00" << endl;
}

TimeStamp::TimeStamp(int year, int month, int day, int hour, int minute): Date(year, month, day)
{
	minutes = Date::getDate() * MINUTES_PER_DAY + hour * MINUTES_PER_HOUR + minute;
}

TimeStamp::~TimeStamp()
{
}

double TimeStamp::countTimeBetweenDatesInMinutes(TimeStamp firstTimeStamp, TimeStamp secondTimeStamp)
{
	double firstTimeStampMinutes = firstTimeStamp.minutes;
	double secondTimeStampMinutes = secondTimeStamp.minutes;

	if (firstTimeStampMinutes > secondTimeStampMinutes) {
		return firstTimeStampMinutes - secondTimeStampMinutes;
	}
	else {
		return secondTimeStampMinutes - firstTimeStampMinutes;
	}
}

void TimeStamp::addSomeMinutes(double howManyMinutes)
{
	minutes = minutes + howManyMinutes;
	estimateNewDateByMinutes(minutes);
}

void TimeStamp::subSomeMinutes(double howManyMinutes)
{
	minutes = minutes - howManyMinutes;
	estimateNewDateByMinutes(minutes);
}

void TimeStamp::estimateNewDateByMinutes(double quantityOfMinutes)
{
	int currentHour = 0;
	int currentMinute = 0;
	int howManyDays = quantityOfMinutes / MINUTES_PER_DAY;
	quantityOfMinutes = quantityOfMinutes - howManyDays * MINUTES_PER_DAY;
	Date::estimateNewDate(howManyDays);
	while (quantityOfMinutes > MINUTES_PER_HOUR) {
		currentHour = currentHour + 1;
		quantityOfMinutes = quantityOfMinutes - MINUTES_PER_HOUR;
	}
	currentMinute = quantityOfMinutes;
	cout << ", godzina " << currentHour << ", minut: " << currentMinute << endl;
}

double TimeStamp::getMinutes()
{
	return minutes;
}

void TimeStamp::setMinutes(double newValueOfMinutes)
{
	minutes = newValueOfMinutes;
}

bool TimeStamp::operator==(TimeStamp comparedTimeStamp)
{
	if (minutes == comparedTimeStamp.minutes) {
		return true;
	}
	else {
		return false;
	}
}

bool TimeStamp::operator<(TimeStamp comparedTimeStamp)
{
	if (minutes < comparedTimeStamp.minutes) {
		return true;
	}
	else {
		return false;
	}
}

bool TimeStamp::operator>(TimeStamp comparedTimeStamp)
{
	if (minutes > comparedTimeStamp.minutes) {
		return true;
	}
	else {
		return false;
	}
}

bool TimeStamp::operator<=(TimeStamp comparedTimeStamp)
{
	if (minutes <= comparedTimeStamp.minutes) {
		return true;
	}
	else {
		return false;
	}
}

bool TimeStamp::operator>=(TimeStamp comparedTimeStamp)
{
	if (minutes >= comparedTimeStamp.minutes) {
		return true;
	}
	else {
		return false;
	}
}


#include "pch.h"
#include "Date.h"
#include <iostream>
#include <vector>

#define DEFAULT_YEAR 1970
#define DEFAULT_MONTH 1
#define DEFAULT_DAY 1
#define JANUARY 1
#define FEBRUARY 2
#define MARCH 3
#define APRIL 4
#define MAY 5
#define JUNE 6
#define JULY 7
#define AUGUST 8
#define SEPTEMBER 9
#define OCTOBER 10
#define NOVEMBER 11
#define DECEMBER 12
#define LONGER_MONTH 31
#define SHORTER_MONTH 30
#define FEBRUARY_IN_LEAP_YEAR 29
#define FEBRUARY_IN_NORMAL_YEAR 28
#define LEAP_YEAR 366
#define NORMAL_YEAR 365


using namespace std;

Date::Date()
{
	date = 0;
	//cout << "Utworzono date rok: 1970, miesiac: 01, dzien: 01" << endl;
}

Date::Date(int year, int month, int day)
{
	date = countQuantityOfDays(year, month, day);
}

Date::~Date()
{
}

double Date::countQuantityOfDays(int year, int month, int day)
{
	double quantityOfDays = day - DEFAULT_DAY;
	double result = 0;

	vector<int> listOfYears;
	vector<int> listOfMonths;
	if (year > DEFAULT_YEAR) {
		for (int i = DEFAULT_YEAR; i < year; i++) {
			listOfYears.push_back(i);
		}
	}
	if (month > DEFAULT_MONTH) {
		for (int i = DEFAULT_MONTH; i < month; i++) {
			listOfMonths.push_back(i);
		}
	}

	for (int i = 0; i < listOfYears.size(); i++) {
		if (checkIsLeap(listOfYears.at(i))) {
			result = result + LEAP_YEAR;
		}
		else {
			result = result + NORMAL_YEAR;
		}
	}

	for (int i = 0; i < listOfMonths.size(); i++) {
		if (checkIsLongerMonth(listOfMonths.at(i))) {
			result = result + LONGER_MONTH;
		}
		else if (checkIsFebruary(listOfMonths.at(i))) {
			if (checkIsLeap(year)) {
				result = result + FEBRUARY_IN_LEAP_YEAR;
			}
			else {
				result = result + FEBRUARY_IN_NORMAL_YEAR;
			}
		}
		else {
			result = result + SHORTER_MONTH;
		}
	}

	result = result + quantityOfDays;

	return result;
}

double Date::countTimeBetweenDates(Date firstDate, Date secondDate)
{
	double firstDateInDays = firstDate.date;
	double secondDateInDays = secondDate.date;

	if (firstDateInDays > secondDateInDays) {
		return firstDateInDays - secondDateInDays;
	}
	else {
		return secondDateInDays - firstDateInDays;
	}
}

int Date::countQuantityOfDaysInCurrentYear(int year)
{
	int quantityOfDaysInCurrentYear = 0;

	if (checkIsLeap(year)) {
		quantityOfDaysInCurrentYear = LEAP_YEAR;
		return quantityOfDaysInCurrentYear;
	}
	else {
		quantityOfDaysInCurrentYear = NORMAL_YEAR;
		return quantityOfDaysInCurrentYear;
	}
}

int Date::countQuantityOfDaysInCurrentMonth(int month)
{
	int quantityOfDaysInCurrentMonth = 0;

	if (checkIsLongerMonth(month)) {
		quantityOfDaysInCurrentMonth = LONGER_MONTH;
		return quantityOfDaysInCurrentMonth;
	}
	else if(checkIsFebruary(month)){
		quantityOfDaysInCurrentMonth = FEBRUARY_IN_NORMAL_YEAR;
		return quantityOfDaysInCurrentMonth;
	}
	else {
		quantityOfDaysInCurrentMonth = SHORTER_MONTH;
		return quantityOfDaysInCurrentMonth;
	}
}

void Date::addSomeDays(int howManyDays)
{
	double currentDate = date;
	currentDate = currentDate + howManyDays;
	date = currentDate;
	estimateNewDate(currentDate);
}

void Date::subSomeDays(int howManyDays)
{
	double currentDate = date;
	currentDate = currentDate - howManyDays;
	if (currentDate > 0) {
		date = currentDate;
		estimateNewDate(currentDate);
	}
	else {
		cout << "ERROR ! DATE IS LOWER THAN DEFAULT DAY";
	}

}

void Date::estimateNewDate(double quantityOfDays)
{
	int currentYear = DEFAULT_YEAR;
	int currentMonth = DEFAULT_MONTH;
	int currentDay = DEFAULT_DAY;
	int quantityOfDaysInCurrentYear = 0;
	int quantityOfDaysInCurrentMonth = 0;
	/*
	while (quantityOfDays > 0) {
		if(countQuantityOfDaysInCurrentYear(currentYear) > quantityOfDays) {
			quantityOfDays = quantityOfDays - countQuantityOfDaysInCurrentYear(currentYear);
			currentYear = currentYear + 1;
		}
		else if (countQuantityOfDaysInCurrentMonth(currentMonth) > quantityOfDays) {
			if (currentMonth == 2 && checkIsLeap(currentYear)) {
				quantityOfDays = quantityOfDays - 1;
			}
			quantityOfDays = quantityOfDays - countQuantityOfDaysInCurrentMonth(currentMonth);
			currentMonth = currentMonth + 1;
		}
	}
	*/
	while (countQuantityOfDaysInCurrentYear(currentYear) <= quantityOfDays) {
		quantityOfDays = quantityOfDays - countQuantityOfDaysInCurrentYear(currentYear);
		currentYear = currentYear + 1;
	}

	while (countQuantityOfDaysInCurrentMonth(currentMonth) <= quantityOfDays) {
		if (currentMonth == FEBRUARY && checkIsLeap(currentYear)) {
			quantityOfDays = quantityOfDays - 1;
		}
		quantityOfDays = quantityOfDays - countQuantityOfDaysInCurrentMonth(currentMonth);
		currentMonth = currentMonth + 1;
	}

	currentDay = currentDay + quantityOfDays;

	
	cout << "Nowa data to: rok: " << currentYear << ", miesiac: " << currentMonth << ", dzien: " << currentDay;
}

bool Date::checkIsLeap(int year)
{
	if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
		return true;
	else
		return false;
}

bool Date::checkIsLongerMonth(int month)
{
	if (month == JANUARY || month == MARCH || month == MAY || month == JULY || month == AUGUST || month == OCTOBER || month == DECEMBER) {
		return true;
	}
	else {
		return false;
	}
}

bool Date::checkIsFebruary(int month)
{
	if (month == FEBRUARY) {
		return true;
	}
	else {
		return false;
	}
}

double Date::getDate()
{
	return date;
}

void Date::setDate(double newDate)
{
	date = newDate;
}

bool Date::operator==(Date comparedDate)
{
	if (date == comparedDate.date) {
		return true;
	}
	else {
		return false;
	}
}

bool Date::operator<(Date comparedDate)
{
	if (date < comparedDate.date) {
		return true;
	}
	else {
		return false;
	}
}

bool Date::operator>(Date comparedDate)
{
	if (date > comparedDate.date) {
		return true;
	}
	else {
		return false;
	}
}

bool Date::operator<=(Date comparedDate)
{
	if (date <= comparedDate.date) {
		return true;
	}
	else {
		return false;
	}
}

bool Date::operator>=(Date comparedDate)
{
	if (date >= comparedDate.date) {
		return true;
	}
	else {
		return false;
	}
}


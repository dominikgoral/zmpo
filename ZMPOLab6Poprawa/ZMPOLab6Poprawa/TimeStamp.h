#pragma once
#include "Date.h"
class TimeStamp :
	public Date
{
public:
	TimeStamp();
	TimeStamp(int year, int month, int day, int hour, int minute);
	~TimeStamp();
	static double countTimeBetweenDatesInMinutes(TimeStamp firstTimeStamp, TimeStamp secondTimeStamp);
	void addSomeMinutes(double howManyMinutes);
	void subSomeMinutes(double howManyMinutes);
	void estimateNewDateByMinutes(double quantityOfMinutes);
	double getMinutes();
	void setMinutes(double newValueOfMinutes);
	bool operator ==(TimeStamp comparedTimeStamp);
	bool operator <(TimeStamp comparedTimeStamp);
	bool operator >(TimeStamp comparedTimeStamp);
	bool operator <=(TimeStamp comparedTimeStamp);
	bool operator >=(TimeStamp comparedTimeStamp);

private:
	double minutes;
};

